# UBank Quiz

This is a NodeJs module to for some Date Range operations like calc number of days, months and check for valid dates between range.

## Quiz Description

![Quiz description](./assets/quiz1.jpg "Quiz description")
![Quiz constraints](./assets/quiz2.jpg "Quiz constraints")

## Instllation

`git clone git@bitbucket.org:Sokkary/ubank-quiz.git && cd ubank-quiz && npm i && npm test`

## Test

`npm test`
