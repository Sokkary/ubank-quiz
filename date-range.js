let isLeap = (year) => new Date(year, 1, 29).getDate() === 29;

const truncate = (date) => {
  if (date.toString().trim().length < 8) {
    throw new Error('Invalid Date!');
  }

  return date.toString().trim().substring(0, 8);
}

const formatDate = (date) => {
  date = truncate(date);

  const year = Number(date.substring(0, 4));
  const month = Number(date.substring(4, 6));
  const day = Number(date.substring(6, 8));

  return { year, month, day };
}

const isValidDate = (date) => {
  try { date = truncate(date); } catch(e) { return false; }

  if (isNaN(Number(date))) {
    return false;
  }

  if (date.length !== 8) {
    return false;
  }

  const year = Number(date.substring(0, 4));
  if (year < 1970) {
    return false;
  }

  const month = Number(date.substring(4, 6));
  if (month > 12 || month < 1) {
    return false;
  }

  const day = Number(date.substring(6, 8));
  if (day > 31 || day < 1) {
    return false;
  }

  if (month === 2 && isLeap(year) && day > 29) {
    return false;
  }

  if (month === 2 && !isLeap(year) && day > 28) {
    return false;
  }

  if ([4, 6, 9, 11].indexOf(month) > -1 && day > 30) {
    return false;
  }

  return true;
}

const validateAndFormat = (from, to) => {
  if (!isValidDate(from) || !isValidDate(to)) {
    throw new Error('Invalid date range!');
  }

  const fromDate = formatDate(from);
  const toDate = formatDate(to);

  return { fromDate, toDate }
}

const days = (from, to) => {
  const { fromDate, toDate } = validateAndFormat(from, to);  
  const diff = new Date(toDate.year, toDate.month, toDate.day).getTime() - new Date(fromDate.year, fromDate.month, fromDate.day).getTime();
  const days = diff / (24 * 60 * 60 * 1000);

  return days + 1;
}

const months = (from, to) => {
  const { fromDate, toDate } = validateAndFormat(from, to);
  const diff = new Date(toDate.year, toDate.month, toDate.day).getTime() - new Date(fromDate.year, fromDate.month, fromDate.day).getTime();
  const months = diff / (30 * 24 * 60 * 60 * 1000);

  return months % 1 > 0 ? Number(months.toFixed(1)) : months;
}

const isBetween = (date, from, to) => {
  const { fromDate, toDate } = validateAndFormat(from, to);

  if (!isValidDate(date)) {
    throw new Error('Invalid date!');
  }

  const d = formatDate(date);
  const time = new Date(d.year, d.month, d.day).getTime();

  if (time <= new Date(toDate.year, toDate.month, toDate.day).getTime() && time >= new Date(fromDate.year, fromDate.month, fromDate.day).getTime()) {
    return true;
  }

  return false;
}

module.exports = {
  truncate,
  isValidDate,
  days,
  months,
  isBetween
}
