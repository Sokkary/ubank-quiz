const expect = require('Chai').expect;
const {
  truncate,
  isValidDate,
  days,
  months,
  isBetween
} = require('./date-range');

describe('DateRange Module', () => {

  describe('days', () => {
    it('should return number of days between 2 dates, including from/to dates within the range', () => {
      const count = days(20101001, 20101005);
      expect(count).equal(5);
    });

    it('should return 1 when from/to are the same', () => {
      const count = days(20101001, 20101005);
      expect(count).equal(5);
    });

    it('should return total days acrross months', () => {
      let count = days(20121015, 20121114);
      expect(count).equal(30);

      count = days(20121115, 20121214);
      expect(count).equal(31);
    });

    it('should return total days acrross years', () => {
      let count = days(20121015, 20131014);
      expect(count).equal(365);
    });

  });

  describe('months', () => {
    it('should return 1 when from/to is exactly 1 month apart', () => {
      const count = days(20101001, 20101005);
      expect(count).equal(5);
    });

    it('should return the left days of the month as fraction compared to the month, ex: 0.5, 0.25, 0.75..etc', () => {
      const count = months(20121001, 20121115);
      expect(count).equal(1.5);
    });

    it('should return only the days of the month as fraction compared to the month if the different is less than 1 month', () => {
      let count = months(20121001, 20121008);
      expect(count).equal(0.2);

      count = months(20121001, 20121015);
      expect(count).equal(0.5);

      count = months(20121001, 20121022);
      expect(count).equal(0.7);
    });

  });

  describe('isBetween', () => {
    it('should return true if date within from/to dates', () => {
      expect(isBetween(20121115, 20121110, 20121120)).to.be.true;
    });

    it('should return false if date is not within from/to dates', () => {
      expect(isBetween(20121121, 20121110, 20121120)).to.be.false;
      expect(isBetween(20121109, 20121110, 20121120)).to.be.false;
    });

    it('should return true if date is equal to either from or to date', () => {
      expect(isBetween(20121110, 20121110, 20121120)).to.be.true;
      expect(isBetween(20121120, 20121110, 20121120)).to.be.true;
    });

  });

  describe('truncate', () => {
    it('should return only the date part from datetime format', () => {
      expect(truncate('20121115 10:06:00')).equal('20121115');
      expect(truncate('2012111510:06:00')).equal('20121115');
    });

    it('should throw if length is less than 8 digits', () => {
      expect(() => truncate('2012111')).to.throw(Error);
    });    
  });

  describe('isValidDate', () => {
    it('should false if date is less than 8 digits', () => {
      expect(isValidDate('2018121')).to.be.false;
    });

    it('should false if date years is less than 1970', () => {
      expect(isValidDate('19691215')).to.be.false;
    });

    it('should false if month is invalid', () => {
      expect(isValidDate('20181315')).to.be.false;
      expect(isValidDate('20180015')).to.be.false;
      expect(isValidDate('2018-1115')).to.be.false;
    });

    it('should false if year is not leap and February is greater than 28 days', () => {
      expect(isValidDate('20180229')).to.be.false;      
    });

    it('should false if year is leap and February is greater than 29 days', () => {
      expect(isValidDate('20160230')).to.be.false;
      expect(isValidDate('20160229')).to.be.true;
    });

    it('should false if day is invalid', () => {
      expect(isValidDate('20181200')).to.be.false;
      expect(isValidDate('20181232')).to.be.false;
    });

    it('should be false if day exceeds max days of month', () => {      
      expect(isValidDate('20180131')).to.be.true;
      expect(isValidDate('20180228')).to.be.true;
      expect(isValidDate('20180331')).to.be.true;
      expect(isValidDate('20180431')).to.be.false;
      expect(isValidDate('20180531')).to.be.true;
      expect(isValidDate('20180631')).to.be.false;
      expect(isValidDate('20180731')).to.be.true;
      expect(isValidDate('20180831')).to.be.true;
      expect(isValidDate('20180931')).to.be.false;
      expect(isValidDate('20181031')).to.be.true;
      expect(isValidDate('20181131')).to.be.false;  
      expect(isValidDate('20181231')).to.be.true;          
    });
  });

});
